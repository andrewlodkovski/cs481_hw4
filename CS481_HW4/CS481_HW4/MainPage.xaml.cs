﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CS481_HW4
{
    public partial class MainPage : ContentPage
    {
        private string delay = "The current order delay is 20 seconds*";
        public string Delay
        {
            get
            {
                return delay;
            }
            set
            {
                delay = value;
                OnPropertyChanged("Delay");
            }
        }

        public Food[] menu = {
            new Food{Name="Spaghetti", Img="spaghetti.jpg", Description="Delicious creamy spaghetti!" },
            new Food{Name="Beef Steak", Img="beef.jpg", Description="Beef steak, perfect for sharing with your friends!" },
            new Food{Name="Shrimp Soup", Img="soup.jpg", Description="Soup with shrimp and vegetables, perfect for cold days!" },
            new Food{Name="Green Tea", Img="tea.jpg", Description="Hot green tea" },
            new Food{Name="Coke", Img="coke.jpg", Description="Cold coke" },
            new Food{Name="Coffee", Img="coffee.jpg", Description="Coffee, hot or cold depending on your choice!" }
        };

        public string[] names = { "Amanda", "Andre", "Julia", "Dmitry", "Vladimir", "Mike", "Diana", "Daniel" };

        private int counter = 1;
        private List<Order> orders;
        private System.Timers.Timer timer;

        public MainPage()
        {
            orders = new List<Order>();
            InitializeComponent();
            BindingContext = this;
            MakeOrder();
            MakeOrder();
            MakeOrder();
            timer = new System.Timers.Timer(20000); //Timer to call MakeOrder every 20 seconds
            timer.Elapsed += OnElapsed;
            timer.AutoReset = true;
            timer.Enabled = true;

        }

        //Timer trigger
        private void OnElapsed(object sender, EventArgs args)
        {
            MakeOrder();
            if(Delay[Delay.Length-1] != '*') Delay += "*";
        }

        private bool MakeOrder()
        {
            ObservableCollection<Food> order = new ObservableCollection<Food>();
            for(int i = 0; i < GenerateRandom(1,8); i++)
            {
                order.Add(menu[GenerateRandom(0,6)]);
            }
            Order newOrder = new Order (counter, names[GenerateRandom(0,names.Length)], order);
            orders.Add(newOrder);
            counter++;
            return true;
        }

        private int GenerateRandom(int min, int max)
        {
            Random random = new Random();
            Thread.Sleep(10); //This random generator is so bad that I have to pause the thread so it doesn't repeat numbers
            return random.Next(min, max);
        }

        public void HandleRefreshing(object sender, EventArgs args)
        {
            //If the user removes all of the components of an order, it automatically gets removed
            for(int i = 0; i < orders.Count; i++)
            {
                if(orders[i].Foods.Count == 0)
                {
                    orders.Remove(orders[i]);
                    i--;
                }
            }
            OrdersList.ItemsSource = new List<Order>(orders); //Copy orders list into Custom View list
            if (Delay[Delay.Length - 1] == '*') Delay = Delay.Substring(0, Delay.Length - 1);
            OrdersList.IsRefreshing = false;
        }

        async void OnItemTapped(object sender, ItemTappedEventArgs args)
        {
            Order order = args.Item as Order;
            await Navigation.PushAsync(new OrderPage(order));
        }

        public void OnValueChanged(object sender, ValueChangedEventArgs args)
        {
            if (timer == null) return;
            //The star in the end means that there is a new order and the page must be refreshed
            bool needsRefresh = Delay[Delay.Length - 1] == '*';
            Delay = "The current order interval is " + Math.Round(args.NewValue).ToString() + " seconds";
            if (needsRefresh) Delay += "*";

            //Update timer interval
            timer.Interval = args.NewValue * 1000;
        }
    }
}
