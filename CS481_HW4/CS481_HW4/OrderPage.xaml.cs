﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW4
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class OrderPage : ContentPage
	{
        private Order order;
        private string orderer = "";
        public string Orderer {
            get
            {
                return orderer;
            }
            set
            {
                orderer = value;
                OnPropertyChanged("Orderer");
            }
        }

		public OrderPage ()
		{
			InitializeComponent ();
		}
        public OrderPage(Order order)
        {
            this.order = order;
            InitializeComponent();
            BindingContext = this;
            Orderer = this.order.Name + "'s order";
            FoodsList.ItemsSource = this.order.Foods;
        }
        
        public void OnDeleteFood(object sender, EventArgs args)
        {
            MenuItem mi = (MenuItem)sender;
            order.Foods.Remove((Food)mi.CommandParameter);
            order.Update();
        }

	}
}