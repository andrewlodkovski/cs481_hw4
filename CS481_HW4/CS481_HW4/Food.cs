﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CS481_HW4
{
    public class Food
    {
        public string Name { get; set; }
        public string Img { get; set; }
        public string Description { get; set; }
        
    }
}
