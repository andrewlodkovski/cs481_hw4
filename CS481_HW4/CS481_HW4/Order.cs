﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace CS481_HW4
{
    public class Order
    {
        public int Number { get; set; }
        public string Name { get; set; }
        public ObservableCollection<Food> Foods { get; set; }
        public string FoodsText { get; set; }

        public Order()
        {

        }

        public Order(int number, string name, ObservableCollection<Food> foods)
        {
            Number = number;
            Name = name;
            Foods = foods;
            Update();
        }

        public void Update()
        {
            FoodsText = "";
            for (int i = 0; i < Foods.Count; i++)
            {
                if (i == Foods.Count - 1)
                {
                    FoodsText += Foods[i].Name;
                }
                else
                {
                    FoodsText += Foods[i].Name + ", ";
                }
                if (FoodsText.Length > 50) break;
            }

            if (FoodsText.Length > 50)
            {
                FoodsText = FoodsText.Substring(0, 50) + "...";
            }

        }
    }
}
